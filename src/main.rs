extern crate nats;
use std::collections::HashMap; // for the options

fn main() {
    println!("--------- NATS prototype --------");

    let mut nats = nats::io::Client::new();
    let mut opts = HashMap::new();

    // PING server every 5 seconds
    // opts.insert("ping_interval", "5");

    // Authenticates and starts the loop which processes the protocol.
    // During parsing, it will trigger the subscription callbacks.
    // Wrap in a try!
    match nats.connect(&mut opts) {
      Ok(()) => println!("Successfully connected!"),
      Err(e) => println!("Failed! {}", e)
    }

    nats.subscribe("workers.results",
                   Box::new(|msg, _| {
                       println!("Results: {}", msg);
                   }));

    nats.subscribe("workers.double",
                   Box::new(|msg, nats| {
                       println!("Doubling: {}", msg);
                       let n = msg.parse::<u8>().unwrap();
                       let result = n * 2;
                       nats.publish("workers.results", result.to_string());
                   }));

    // subscriptions should double the number
    nats.publish("workers.double", "10".to_string());

    // TODO: Change into readline from stdin and break
    loop {}

    // stops the connection and cleans state
    // nats.close();
}
