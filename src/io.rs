
// Protocol
const CONNECT: &'static str = "CONNECT";
const INFO:    &'static str = "INFO";
const PING:    &'static str = "PING\r\n";
const PONG:    &'static str = "PONG\r\n";
const PUB:     &'static str = "PUB";
const SUB:     &'static str = "SUB";
const UNSUB:   &'static str = "UNSUB";
const MSG:     &'static str = "MSG";
const OK:      &'static str = "+OK\r\n";
const ERR:     &'static str = "-ERR";
const CR_LF:   &'static str = "\r\n";
const SPC:     &'static str = " ";

extern crate rustc_serialize as serialize;

use std::collections::HashMap;
use std::io::BufRead;
use std::io::BufStream;
use std::io::Read;
use std::io::Write;
use std::net::TcpStream;
use std::sync::{Arc, Mutex};
use std::thread;

// Local
use parser::Parser;
pub struct Client {
    io: Arc<Mutex<BufStream<TcpStream>>>,
    options:  HashMap<&'static str, &'static str>,
    ssid: u8,
    subs: HashMap<u8, Box<Fn(&str, &Client)>>
}

impl Client {


    pub fn new() -> Client {

        let stream = TcpStream::connect("192.168.0.2:4222").unwrap();
        let mut bufstream = BufStream::new(stream);
        let mut io = Arc::new(Mutex::new(bufstream));

        return Client {
            io: io,
            options: HashMap::new(),
	    ssid: 1,
	    subs: HashMap::new(),
        };
    }

    pub fn connect(&self, options: &mut HashMap<&str, &str>) -> Result<(), &str> {
        println!("Connecting...");

	// Try to parse or return Nats::ConfigError(e)
	// Set a default
	if !options.contains_key("ping_interval") {
	  options.insert("ping_interval", "120");
	}

	println!("Will print every: {} seconds", options["ping_interval"]);

	// Starting the parser loop, let it run....
        Parser::process_protocol(&self, self.io.clone());

	Ok(())
    }

    // Gracefully exit...
    pub fn close(&self) {
      println!("NATS client stopping...");
    }

    pub fn publish(&self, subject: &str, message: String) {
	let msg_size = message.len();
	let pub_op = format!("{} {} {}{}{}{}", PUB, subject, msg_size, CR_LF, message, CR_LF);
        println!("{}", pub_op);
	Client::send_command(pub_op.to_string(), self.io.clone());
    }


    pub fn subscribe(&mut self, subject: &str, subcb: Box<Fn(&str, &Client)>) {
        self.ssid += 1; // TODO: maybe should be an Arc as well??

        println!("SUB {} {} {}", subject, " ", self.ssid);
	let sub_op = format!("{} {} {}{}", SUB, subject, self.ssid, CR_LF);
	println!("{}", sub_op);
	Client::send_command(sub_op.to_string(), self.io.clone());
	self.subs.insert(self.ssid, subcb);
    }


    pub fn send_command(command: String, eio: Arc<Mutex<BufStream<TcpStream>>>) {
        let mut nats_io = eio.clone();
        let mut io = nats_io.lock().unwrap();
        let send_result = write!(io, "{}", command);

        match send_result {
            Err(e) => panic!("Failed to connect! {}", e),
            Ok(_) => {
                println!("Command sent!");
            }
        }

        io.flush().unwrap();
    }

  pub fn read_message_payload(msg_size: u64, eio: Arc<Mutex<BufStream<TcpStream>>>) -> String {

      println!("Will try to read the next line.....");
      let mut nats_io = eio.clone();
      let mut io = nats_io.lock().unwrap();

      let mut line = String::new();

      // TODO: Figure out how to get N bytes
      let taker = BufReader::new(io);

      // gets stuck before here
      println!("Read the message!");
      return line;
  }
  pub fn process_message(&self) {
    println!("This is ok....");
  }
}
