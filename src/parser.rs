
// Protocol
const CONNECT: &'static str = "CONNECT";
const INFO:    &'static str = "INFO";
const PING:    &'static str = "PING\r\n";
const PONG:    &'static str = "PONG\r\n";
const PUB:     &'static str = "PUB";
const SUB:     &'static str = "SUB";
const UNSUB:   &'static str = "UNSUB";
const MSG:     &'static str = "MSG";
const OK:      &'static str = "+OK\r\n";
const ERR:     &'static str = "-ERR";
const CR_LF:   &'static str = "\r\n";
const SPC:     &'static str = " ";

extern crate rustc_serialize as serialize;

use std::net::TcpStream;
use std::io::BufStream;
use std::io::BufRead;
use std::io::Read;
use std::io::Write;
use std::sync::{Arc, Mutex};
use std::thread;

// Local
use io::Client;
pub struct Parser;

impl Parser {


    pub fn process_protocol(nats: &Client, eio: Arc<Mutex<BufStream<TcpStream>>>) {
        println!("Starting the loop...");
        // let mut buf = String::new();

        // How to join the thread??
        thread::spawn(move || {
            loop {
                // Acquire the io resource
                let mut nats_io = eio.clone();

                let mut line = String::new();

		//let result = {
                //  let mut io = nats_io.lock().unwrap();
                //  io.read_line(&mut line).unwrap()
		//};

		let mut io = nats_io.lock().unwrap();
		let result = io.read_line(&mut line).unwrap();

		// Skip empty lines
		if line.len() < 1 {
		  continue
		}

		let mut proto = line.splitn(2, " ");
		let nats_op = proto.nth(0);

		// if handling a message then we have to process the next line as the payload
		// and keep the buffer, then at the end reset the buffer
		// AWAITING_MSG_PAYLOAD or AWAITING_CONTROL
                match nats_op {
		  Some(INFO) => { /* reconnect in case no connection? */ },
		  Some(PING) => {
		    println!("Pinging back...");
		    Client::send_command(PING.to_string(), eio.clone());
		  },
		  Some(PONG) => {
		    println!("Got pong from server!");
		    Client::send_command(PONG.to_string(), eio.clone());
		  },
		  Some(MSG)  => {
		    println!("Handling MSG!!!: {}", line);

		    let msg_op: Vec<&str> = line.split(" ").collect();
		    let mut msg_op_count = msg_op.len();
		    match msg_op_count {
		      4 => {
  		        // PUB: MSG workers.double 3 8
		        println!("Simple PUB");
		        let msg_subject = msg_op[1];
		        let msg_sub_id = msg_op[2];
		        let mut msg_size = msg_op[3].trim().parse::<u64>().unwrap();
		        println!("Reading MSG payload for: {}", msg_subject);

			let mut msg_payload = Client::read_message_payload(msg_size, eio.clone());
			
			// let taker = io2.take(msg_size);
			// let take_result = {
			// let mut io = nats_io.lock().unwrap();
			// io.take(2);
			// };

			
			// io.read_line(&mut msg_payload).unwrap();

		        // let msg_payload_take = 
			// msg_payload_take.read_to_string(&mut msg_payload);
			// println!("wooooo: {:?}", msg_payload);
		        println!("Got: {}", msg_payload);
		      },
		      5 => {
		        // REQ: MSG workers.double 3 _INBOX.aa8493b6562cd616e899f86147 8
		        println!("Request Pattern");
			let msg_subject = msg_op[1];
			let msg_sub_id = msg_op[2];
			let msg_inbox = msg_op[3];
			let msg_size = msg_op[4];
			println!("Reading MSG payload for: {}", msg_subject);
		      },
		      _ => {
		        println!("Malformed MSG: {}", msg_op_count);
		      },
		    }
		  },
		  Some(OK)   => { /* ignore */ },
		  Some(ERR)  => println!("Error in the protocol"),
		  Some(_)    => println!("Unknown Protocol: {}", line),
		  None       => println!("No Protocol: {}", line),
		}

                // Parse the protocol and dispatch proper action
                // if we get PING, send PONG
                // every interval, we will dispatch our own PING
		// that might be on the client side responsibility
            }
        });

    }

}
